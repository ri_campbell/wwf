using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WWF
{
    static class Dictionary
    {
        static string txt = Properties.Resources.Dictionary___Lengths;
        static string txtRev = Properties.Resources.Dictionary___Lengths___Reverse;
        
        public static ILookup<int, string> Dict = ExtensionMethods.ToLookup(Regex.Split(txt, "\r\n").ToList()); 
        public static ILookup<int, string> DictRev = ExtensionMethods.ToLookup(Regex.Split(txtRev, "\r\n").ToList());

        public static List<string> GetWordsOfLength(int lowerLength, int upperLength,  Direction direction)
        {
            var list = new List<string>();

            for (int length = lowerLength; length < upperLength + 1; length++)
            {
                if (direction == Direction.Down)
                {
                    foreach (var word in Dict[length])
                    {
                        list.Add(word);
                    }
                }
                else if (direction == Direction.Across)
                {
                    foreach (var word in DictRev[length])
                    {
                        list.Add(word);
                    }
                }
            }

            return list;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace WWF
{
    public class Grid
    {
        private readonly Square[,] _grid;
        
        public Grid(int gridSize)
        {
            _grid = new Square[gridSize, gridSize];

            var count = 0;
            for (var x = 0; x < gridSize; x++)
            {
                for (var y = 0; y < gridSize; y++)
                {
                    _grid[x, y] = new Square();
                    _grid[x, y].LetterBonus = Constants.DoubleLetter.Contains(count) ? 2 : Constants.TripleLetter.Contains(count) ? 3 : 1;
                    _grid[x, y].WordBonus = Constants.DoubleWord.Contains(count) ? 2 : Constants.TripleWord.Contains(count) ? 3 : 1;
                    count ++;
                }
            }
        }

        public Grid(List<List<Square>> squares) //Used in WWF.Testing only
        {         
            _grid = new Square[squares.Count, squares.Count];

            var size = squares.Count;
            
            for (var x = 0; x < size; x++)
            {
                for (var y = 0; y < size; y++)
                {
                    _grid[x, y] = squares[y][x];
                }
            }
        }

        public Square GetSquare(int x, int y)
        {
            return _grid[x, y];
        }

        public void SetSquare(int x, int y, Square square)
        {
            _grid[x, y] = square;
        }

        public Grid RotateClockwise()
        {
            var rotatedGrid = new Grid(_grid.GetLength(0));

            for (var r = 0; r < _grid.GetLength(0); r++)
            {
                for (var c = 0; c < _grid.GetLength(1); c++)
                {
                    rotatedGrid.SetSquare(r, c, _grid[_grid.GetLength(1) - 1 - c, r]);
                }
            }

            return rotatedGrid;
        }

        public Limits GetLimits()
        {
            var size = _grid.GetLength(0);
            var limits = new Limits(size - 1, 0, size - 1, 0);

            for (var r = 0; r < size; r++)
            {
                for (var c = 0; c < size; c++)
                {
                    var chr = char.IsLetter(_grid[r, c].Letter);
                    if (chr && c <= limits.LeftColumn) { limits.LeftColumn = Math.Max(c - 1, 0); }
                    if (chr && c >= limits.RightColumn) { limits.RightColumn = Math.Min(c + 1, size - 1); }
                    if (chr && r <= limits.UpperRow) { limits.UpperRow = Math.Max(r - 1, 0); }
                    if (chr && r >= limits.LowerRow) { limits.LowerRow = Math.Min(r + 1, size - 1); }
                }
            }

            return limits;
        }
    }
}

using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace WWF
{
    class Board
    {
        public Grid BoardFilled;

        public Board(int boardSize)
        {
            BoardFilled = new Grid(boardSize);

            var letters = new List<string>
            {
                //012345678901234
                "               ", //0
                "               ", //1
                "               ", //2
                "               ", //3
                "               ", //4
                "               ", //5
                "choleric       ", //6
                "       a       ", //7 M
                "     bottle    ", //8
                "               ", //9
                "               ", //10
                "               ", //11
                "               ", //12
                "               ", //13
                "               ", //14
                //012345678901234
            };

            for (var i = 0; i < boardSize; i++)
            {
                var chrs = letters[i].ToLowerInvariant().ToCharArray().ToList();
                for (var j = 0; j < boardSize; j++)
                {
                    BoardFilled.GetSquare(i, j).Row = i;
                    BoardFilled.GetSquare(i, j).Column = j;
                    BoardFilled.GetSquare(i, j).Letter = chrs[j];
                    BoardFilled.GetSquare(i, j).Score = Program.LetterValues(BoardFilled.GetSquare(i, j).Letter.ToString(CultureInfo.InvariantCulture).ToCharArray().ToList())[0]; //TODO: !!                     
                }
            }
        }
    }
}
﻿namespace WWF
{
    internal class Constants
    {
        public const char Blank = ' ';

        //Absolute board references
        public static readonly int[] DoubleLetter = { 17, 27, 31, 34, 40, 43, 62, 66, 68, 72, 94, 100, 124, 130, 152, 156, 158, 162, 181, 184, 190, 193, 197, 207 };
        public static readonly int[] TripleLetter = { 6, 8, 48, 56, 80, 84, 90, 104, 120, 134, 140, 144, 168, 176, 216, 218 };
        public static readonly int[] DoubleWord = { 20, 24, 52, 76, 88, 108, 116, 136, 148, 172, 200, 204 };
        public static readonly int[] TripleWord = { 3, 11, 45, 59, 165, 179, 213, 221 };
    }
}

﻿namespace WWF
{
    public class Limits
    {
        public int LeftColumn { get; set; }

        public int RightColumn { get; set; }

        public int UpperRow { get; set; }

        public int LowerRow { get; set; }

        public Limits(int l, int r, int u, int b)
        {
            LeftColumn = l;
            RightColumn = r;
            UpperRow = u;
            LowerRow = b;
        }
    }
}

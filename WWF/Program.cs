﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace WWF
{
    class Program
    {
        private const int BoardSize = 15;

        static public void Main(string[] args)
        {
            var board = new Board(BoardSize).BoardFilled;
            //board = GetBoard(board); //For board letter input by user.

            WriteBoard(board);

            var letters = GetLetters();

            var startTime = DateTime.Now;

            var words = new List<Word>();

            words.AddRange(SquareRunThrough(board, letters, Direction.Down)); //Down words

            board = board.RotateClockwise();

            words.AddRange(SquareRunThrough(board, letters, Direction.Across)); //Across words
            
            var endTime = DateTime.Now;

            var wordsOrdered = from element in words
                               orderby element.Row, element.Column, element.WordScore descending 
                               select element;

            WriteWords(wordsOrdered.ToList());

            Console.WriteLine("Time = {0} seconds", Math.Round((endTime - startTime).TotalSeconds, 2));

            Console.ReadKey();
        }

        static Grid GetBoard(Grid board)
        {
            var letterFrequencyTotal = new List<LetterFrequency>(); //Cumulative letter frequency for board. Max # of each tile allowed.
            for (var row = 0; row < BoardSize; row++)
            {
                for (; ; )
                {
                    var letterFrequencyRow = new List<LetterFrequency>(letterFrequencyTotal.Count);

                    letterFrequencyRow.AddRange(letterFrequencyTotal.Select(i => new LetterFrequency {Char = i.Char, Count = i.Count, MaxAllowed = i.MaxAllowed }));
                    
                    Console.Write("Enter Row {0} letters: ", row + 1);
                    var rowLetters = (Console.ReadLine().ToLower().ToList());

                    if (rowLetters.Count < BoardSize) //Fill rest of row with blanks
                    {
                        for (var j = rowLetters.Count; j < BoardSize; j++)
                        {
                            rowLetters.Add(Constants.Blank);
                        }
                    }

                    letterFrequencyRow = LetterFreq(rowLetters, letterFrequencyRow, BoardSize); //Get letter frequencies of current row

                    if (!LettersCheck(rowLetters, letterFrequencyRow, BoardSize)) { continue; } //Check validity of entered characters

                    letterFrequencyTotal = letterFrequencyRow;

                    for (var column = 0; column < BoardSize; column++) //Add row letters to board
                    {
                        board.GetSquare(row, column).Letter = rowLetters[column];
                        board.GetSquare(row, column).Score = LetterValues(rowLetters[column].ToString(CultureInfo.InvariantCulture).ToCharArray().ToList())[0];
                    }

                    break;
                }
            }
            
            return board;
        }

        static void WriteBoard(Grid board)
        {
            Console.WriteLine(" 123456789012345");
            for (var i = 0; i < BoardSize; i++)
            {
                Console.Write("|");
                for (var j = 0; j < BoardSize - 1; j++)
                {
                    Console.Write((board.GetSquare(i, j)).Letter.ToString(CultureInfo.InvariantCulture).ToUpper());
                }
                Console.WriteLine((board.GetSquare(i, BoardSize - 1)).Letter.ToString(CultureInfo.InvariantCulture).ToUpper() + "| {0}", i + 1);
            }
        }

        static void WriteWords(List<Word> words )
        {
            foreach (var i in words)
            {
                if (i.Letters.Count > 0) //Can use to restrict results
                {
                    Console.WriteLine("({0},{1}) {2}\t {3}\t Total Score: {4}", i.Row + 1, i.Column + 1, i.Direction, string.Concat(i.Letters).ToUpper(), i.WordScore);
                }
            }
            Console.WriteLine(words.Count + " words");
        }

        static List<char> GetLetters()
        {
            List<char> letters;
            
            for (; ; )
            {
                var letFreq = new List<LetterFrequency>();
                Console.Write("Enter letters: ");
                letters = Console.ReadLine().ToLower().ToCharArray().ToList();
                
                if (letters.Count == 0) { continue; }

                letFreq = LetterFreq(letters, letFreq, 7);

                if (!LettersCheck(letters, letFreq, 7)) { continue; }

                break;
            }
            return (letters);
        }
        
        static List<Word> SquareRunThrough(Grid board, List<char> letters, Direction direction)
        {
            var words = new List<Word>();

            var limits = board.GetLimits(); //Limits let code ignore checking large sections of board  
            
            for (var row = 0; row < limits.LowerRow; row++)
            {
                for (var column = limits.LeftColumn; column < limits.RightColumn + 1; column++)
                {
                    var contactRow = BoardSize;

                    if (!CheckSquare(board, row, Math.Min(row + letters.Count - 1, BoardSize - 1), column, ref contactRow))
                    {
                        continue;
                    }

                    words.AddRange(WordSolver(board, letters, row, column, contactRow, direction));
                }
            }

            var wordsOrdered = from element in words //Order across words by square (down words ordered incidentally)
                orderby element.Row 
                select element;

            return wordsOrdered.ToList();
        }

        static bool CheckSquare(Grid grid, int top, int bottom, int column, ref int contactRow) //Check for tiles which disqualify square (1,2), otherwise for a connection which qualifies it (3)
        {
            if ((!grid.GetSquare(Math.Max(top - 1, 0), column).IsBlank && top > 0)) // 1)Check square above for letter
                { return false;}

            for (var row = top; row < BoardSize; row++) // 2)Check for continuous column of letters to bottom (Includes bottom row)
            {
                if (grid.GetSquare(row, column).IsBlank)
                {
                    break;
                }
                if (!grid.GetSquare(row, column).IsBlank && row == BoardSize - 1)
                {
                    return false;
                }
            }

            for (var row = top; row < bottom + 1; row++) // 3)Check letters-by-3 box (squares in column and adjacent columns on either side)
            {
                if (grid.GetSquare(row, Math.Max(column - 1, 0)).Letter.ToString(CultureInfo.InvariantCulture) + grid.GetSquare(row, column).Letter + grid.GetSquare(row, Math.Min(BoardSize - 1, column + 1)).Letter != "   ")
                {
                    contactRow = row - top + 1;
                    return true;
                }
            }

            if (bottom != BoardSize - 1 && !grid.GetSquare(bottom + 1, column).IsBlank) // 3)Check square below bottom-most reach 
            {
                contactRow = bottom - top + 2;
                return true;
            }
            
            return false;
        }

        static List<Word> WordSolver(Grid grid, List<char> letters, int row, int column, int contactRow, Direction direction)
        {
            var mould = new Mould(grid, letters.Count, row, column, BoardSize); //Find 'mould' (line of empty and filled squares down or across) for current square, eg. "  C D"

            var words = new List<Word>();

            var lowerLength = contactRow; //Check only dictionary words of lengths in applicable range
            var upperLength = mould.Count; //TODO: This can be made more efficient, eg. **CAT should only check words of length 5, not 3 to 5
            var dictionary = Dictionary.GetWordsOfLength(lowerLength, upperLength, Direction.Down);
            var perpLetters = new List<KeyValuePair<int, char>>(); //Record letters that can't occur at specific index in mould as they create illegal perpendicular words

            foreach (var dictWord in dictionary) 
            {
                if (dictWord.Length > mould.Count) { continue; }

                var word = dictWord.ToCharArray().ToList();

                if (!ExtensionMethods.PerpLetterCheck(word, perpLetters)) { continue; }

                var blankLetters = new List<char>();

                if (!mould.MouldFits(word) || !mould.WordFits(contactRow, letters, word, ref blankLetters) || !PerpendicularWords(grid, word, row, column, direction, ref perpLetters))
                {
                    continue;
                }

                //Blank tiles have 0 score. Thus when used for a letter that occurs more than once multiple words must be recored, e.g LOSSE#, LOS#ES and LO#SES
                var wordBlanks = mould.GenerateBlankVariations(word, blankLetters);

                foreach (var w in wordBlanks) //Add each word to final list
                {
                    words.Add(new Word
                    {
                        Row = row,
                        Column = column,
                        Direction = direction,
                        Letters = word,
                        Wrd = string.Concat(word),
                        Scores = LetterValues(w),
                    });
                }
            }

            for (var w = 0; w < words.Count; w++) //Score each word
            {
                words[w] = WordScorer(words[w], grid); 
                if (direction == Direction.Across) //Reset coordinates of across words due to rotation
                {
                    var temp = words[w].Row;
                    words[w].Row = BoardSize - 1 - words[w].Column;
                    words[w].Column = temp;
                }
            }

            return words;
        }
        
        static bool PerpendicularWords(Grid grid, List<char> word, int row, int column, Direction direction, ref List<KeyValuePair<int, char>> perpLetters ) //Check any perpendicular words are legal
        {
            for (var r = 0; r < word.Count; r++) //Check perp word at each letter
            {
                var perpWord = word[r].ToString(CultureInfo.InvariantCulture);
                for (var c = column - 1; c > -1; c--) //Construct perp word with letters to left
                {
                    if (grid.GetSquare(r + row, c).IsBlank) { break; }
                    perpWord = grid.GetSquare(r + row, c).Letter + perpWord;
                }
                for (var c = column + 1; c < BoardSize; c++) //Add letters to right
                {
                    if (grid.GetSquare(r + row, c).IsBlank) { break; }
                    perpWord += grid.GetSquare(r + row, c).Letter.ToString(CultureInfo.InvariantCulture);
                }

                if (perpWord.Length == 1) { continue; } //I.E. no adjacent letters

                var key = int.Parse(perpWord.Length.ToString(CultureInfo.InvariantCulture) + Convert.ToInt32(perpWord.ToCharArray()[0]));
                var dictionary = direction == Direction.Down ? Dictionary.Dict[key] : Dictionary.DictRev[key];
                
                if (!dictionary.Contains(perpWord))
                {
                    perpLetters.Add(new KeyValuePair<int, char>(r, word[r]));
                    return false;
                }
            }

            return true;
        }

        static List<LetterFrequency> LetterFreq(List<char> letters, List<LetterFrequency> letterFrequency, int length) //Returns list of letter frequencies [char, count, max allowed, char, count, max allowed, etc.]
        {
            for (var letter = 0; letter < letters.Count; letter ++)
            {
                if ((Convert.ToInt32(letters[letter]) < 97 || Convert.ToInt32(letters[letter]) > 122) && Convert.ToInt32(letters[letter]) != 32){ continue; } //TODO:
                var duplicate = 0;
                var index = 0;
                foreach (var let in letterFrequency)
                {
                    if (let.Char == Convert.ToInt32(letters[letter]))
                    {
                        duplicate += 1;
                        index = letterFrequency.IndexOf(let);
                        break;
                    }
                }

                if (duplicate > 0)
                {
                    letterFrequency[index].Count += 1;
                }
                else
                {
                    letterFrequency.Add(new LetterFrequency
                    {
                        Char = letters[letter],
                        Count = 1,
                        MaxAllowed = 0
                    });
                }
            }

            foreach (var let in letterFrequency)
            {
                switch (let.Char) //Number of each tiles allowed
                {
                    case 'j': case 'k': case 'q': case 'x': case 'z':
                        let.MaxAllowed = 1; break;
                    case ' ': case 'b': case 'c': case 'f': case 'm': case 'p': case 'v': case 'w': case 'y':
                        let.MaxAllowed = 2; break;
                    case 'g': 
                        let.MaxAllowed = 3; break;
                    case 'h': case 'l': case 'u'    :
                        let.MaxAllowed = 4; break;
                    case 'd': case 'n': case 's': 
                        let.MaxAllowed = 5; break;
                    case 'r':
                        let.MaxAllowed = 6; break;
                    default:
                        let.MaxAllowed = -1; break;
                }

                if (length == BoardSize)
                {
                    switch (let.Char)
                    {
                        case ' ':
                            let.MaxAllowed = -1; break;
                        case 't':
                            let.MaxAllowed = 7; break;
                        case 'i': case 'o':
                            let.MaxAllowed = 8; break;
                        case 'a':
                            let.MaxAllowed = 9; break;
                        case 'e':
                            let.MaxAllowed = 13; break;
                    }
                }
            }

            return (letterFrequency);
        }

        static bool LettersCheck(List<char> charLetters, List<LetterFrequency> letterFrequency, int length) //Used for both rack tiles(7) and board row tiles(15). Provides every applicable error message.
        {
            var pass = true;

            foreach (var c in charLetters)
            {
                if (!char.IsLetter(c) && c != Constants.Blank)
                {
                    Console.WriteLine("Letters only!");
                    pass = false;
                    break;
                }
            }

            if (charLetters.Count > length)
            {
                if (length == 7)
                {
                    Console.WriteLine("Maximum of 7 tiles allowed!");
                }
                else if (length == BoardSize)
                {
                    Console.WriteLine("Maximum of {0} allowed!", BoardSize);
                }
                pass = false;
            }

            foreach (var letter in letterFrequency)
            {
                if (letter.HasTooManyBlankTiles(length))
                {
                    pass = false;
                }
                else if (letter.HasTooManySingleTiles())
                {
                    pass = false;
                }
                else if (letter.HasTooManyTiles(length))
                {
                    pass = false;
                }
            }

            return pass;
        }

        public static List<int> LetterValues(List<char> word)
        {
            return word.Select(LetterScorer.GetScore).ToList();
        }

        static Word WordScorer(Word word, Grid grid)
        {
            var totalScore = 0;
            var totalWordMultiplier = 1; //Total word multiplier for base word score
            var baseScore = 0; //Basic score of actual word before final word multiplication
            var letterCount = 0; //Check if all letters are used for 35 bonus

            for (var r = 0; r < word.Letters.Count; r++)
            {
                var letterMultiplier = grid.GetSquare(r + word.Row, word.Column).IsBlank ? grid.GetSquare(r + word.Row, word.Column).LetterBonus : 1; //Determine if square letter muliplier is applicable
                var wordMultiplier = grid.GetSquare(r + word.Row, word.Column).IsBlank ? grid.GetSquare(r + word.Row, word.Column).WordBonus : 1; //Determine if square word multiplier is applicable

                totalWordMultiplier *= wordMultiplier; //Add square word multiplier to total word multiplier

                baseScore += word.Scores[r] * letterMultiplier; //Add current letter score to base word score

                if (!grid.GetSquare(r + word.Row, word.Column).IsBlank) { continue; } //Existing letter - perpendicular word doesn't score

                letterCount++;

                var crossWord = word.Scores[r] * letterMultiplier; //Letter value times letter bonus

                var count = 0; //Count of perpendicular tiles. Needed due to potential blank tiles with 0 score.
                for (var c = word.Column - 1; c > -1; c--) //Add letter scores for perp word to left
                {
                    if (grid.GetSquare(r + word.Row, c).IsBlank) { break; }
                    crossWord += grid.GetSquare(r + word.Row, c).Score;
                    count++;
                }
                for (var c = word.Column + 1; c < BoardSize; c++) //Add letter scores for perp word to right
                {
                    if (grid.GetSquare(r + word.Row, c).IsBlank) { break; }
                    crossWord += grid.GetSquare(r + word.Row, c).Score;
                    count++;
                }
                if (count == 0) { continue; } //No perpendicular word  

                totalScore += crossWord * wordMultiplier; //Add each perp word score to total score
            }

            totalScore += baseScore * totalWordMultiplier; //Add base word score to total score
            if (letterCount == 7) { totalScore += 35; }
            word.WordScore = totalScore;

            return word;
        }
    }
}

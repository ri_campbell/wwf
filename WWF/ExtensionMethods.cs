﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace WWF
{
    public static class ExtensionMethods
    {
        public static bool HasTooManyBlankTiles(this LetterFrequency letter, int length)
        {
            if (letter.Char == ' ' && letter.Count > letter.MaxAllowed && length == 7)
            {
                Console.WriteLine("Maximum of 2 blank tiles allowed!");
                return true;
            }
            return false;
        }

        public static bool HasTooManySingleTiles(this LetterFrequency letter)
        {
            if ((letter.Char == 'j' || letter.Char == 'k' || letter.Char == 'q' || letter.Char == 'x' || letter.Char == 'z') && letter.Count > letter.MaxAllowed)
            {
                Console.WriteLine("Maximum of {0} {1} tile allowed!", letter.MaxAllowed, letter.Char);
                return true;
            }
            return false;
        }

        public static bool HasTooManyTiles(this LetterFrequency letter, int length)
        {
            if ((letter.Char != 'a' && letter.Char != 'e' && letter.Char != 'i' && letter.Char != 'o' && letter.Char != 't') && letter.Count > letter.MaxAllowed && length == 7)
            {
                Console.WriteLine("Maximum of {0} {1} tiles allowed!", letter.MaxAllowed, letter.Char);
                return true;
            }
            if (letter.Char != ' ' && letter.Count > letter.MaxAllowed && length == 15)
            {
                Console.WriteLine("Maximum of {0} {1} tiles allowed!", letter.MaxAllowed, letter.Char);
                return true;
            }
            return false;
        }

        public static ILookup<int, string> ToLookup(List<string> dictionaryList)
        {
            var dict = new List<KeyValuePair<int, string>>();

            foreach (var word in dictionaryList)
            {
                dict.Add(new KeyValuePair<int, string>(word.Length, word));
                //Groupings by key[length + first letter char code]. For use with perpendicular word check.
                dict.Add(new KeyValuePair<int, string>(int.Parse(word.Length.ToString(CultureInfo.InvariantCulture) + Convert.ToInt32(word.ToCharArray()[0])), word));
            }

            var dictionary = dict.ToLookup(kvp => kvp.Key, kvp => kvp.Value);
            
            return dictionary;
        }

        public static bool PerpLetterCheck(List<char> word, List<KeyValuePair<int, char>> perpLetters)
        {
            foreach (var letter in perpLetters)
            {
                if (word[letter.Key] == letter.Value)
                {
                    return false;
                }
            }

            return true;
        }
    }
}

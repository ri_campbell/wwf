﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WWF
{
    public class LetterFrequency
    {
        public char Char { get; set; }

        public int Count { get; set; }

        public int MaxAllowed { get; set; }
    }
}
